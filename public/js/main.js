/** Editable rows and columns */
const GRID = 24;
/** Tile size, pixels squared */
const SIZE = 64;

/**
 * Returns a number indicating the direction
 * to face while placing to get the desired
 * rotation (2nd index) from the desired
 * perspective (1st index).
 * 
 * Return values:
 * ```
 * 0 'north'
 * 1 'east'
 * 2 'south'
 * 3 'west'
 * ```
 */
const MAP = [
    /*      1  2
         9  8  7
      0  0  0  0 */
    [ 1, 0, 3, 2 ], // N
    [ 2, 1, 0, 3 ], // E, Dn
    [ 3, 2, 1, 0 ], // S
    [ 0, 3, 2, 1 ], // W
    [ 0, 1, 2, 3 ]  // Up   
];

/**
 * Takes a raw angle and determines the lowest rotation clockwise
 * to reach the same orientation
 * @param {number} angle Raw angle [deg CW]
 * @returns {number} Fewest degrees clockwise
 */
function netRotation (angle) {
    let result = angle % 360;
    return (result >= 0)? result : result + 360;
}

/**
 * Returns the cardinal direction one must face to get the block to
 * appear as desired.
 * @param {number} perspective Direction from which the block is viewed
 * @param {number} rotation Rotation from default image in [deg CW]
 * @returns {string} Direction to face during placement
 */
function getDirection (perspective, rotation) {
    switch (MAP [perspective] [Math.round(netRotation(rotation) / 90)]) {
        case 0: return 'N';
        case 1: return 'E';
        case 2: return 'S';
        case 3: return 'W';
    }
}

/** Generate the grid of tiles */
function gridDataInit () {
    /** Return value */
    let data = [];
    /** Row counter */
    let i;
    /** Column counter */
    let j;
    for (i = 0; i < GRID; i ++) {
        data.push([]);
        for (j = 0; j < GRID; j ++) {
            data[i].push({
                phi: 0,
                img: null
            });
        }
    }
    return data;
}

document.addEventListener('DOMContentLoaded', (e) => {

    /* * * * * * * * * *
     * INITIALIZE GRID
     */

    /** Visible width */
    const width = SIZE * 14;
    /** Visible height */
    const height = SIZE * 12;
    /** The editing field */
    const grid = d3.select('#grid').append('svg')
        .attr('height', height)
        .attr('width', width);
    /** Panning behavior */
    const zoom = d3.zoom()
        .scaleExtent([0.66, 2.5])
        .translateExtent([[-SIZE * 5, -SIZE * 6], [width + SIZE * 5, height + SIZE * 6]])
        .on('zoom', zoomed)
    /** Horziontal Scale */
    const x = d3.scaleLinear()
        .domain([1, width])
        .range([1, width]);
    /** Vertical Scale */
    const y = d3.scaleLinear()
        .domain([1, height])
        .range([1, height]);
    /** x-axis markers */
    const xAxis = d3.axisBottom(x)
        .ticks(1)
        .tickSize(0)
    /** y-axis markers */
    const yAxis = d3.axisRight(y)
        .ticks(1)
        .tickSize(0)
    /** All rows */
    let rows;
    /** All editable tile groups */
    let tiles;
    /** x-axis labels */
    let gX;
    /** y-axis labels */
    let gY;
    /** Text boxes */
    let text;
    loadPattern(gridDataInit());
    // Apply zooming behavior
    grid.call(zoom)
        .on('dblclick.zoom', null);;
    // No axes
    d3.selectAll('g path')
        .attr('stroke', 'none');
    /** Right-click instructions */
    const rClick = d3.select('#right-click');
    /** Left-click instructions */
    const  lClick = d3.select('#left-click');
    /** Middle-click instructions */
    const mClick = d3.select('#middle-click');
    /** Tile selection grid */
    const tileSelector = d3.select('#tile-selector')

    let instructionsState;
    let resetState;
    instructionsReset();

    /** More zooming behavior */
    function zoomed() {
        tiles.attr('transform', d3.event.transform);
        gX.call(xAxis.scale(d3.event.transform.rescaleX(x)));
        gY.call(yAxis.scale(d3.event.transform.rescaleY(y)));
    }

    /** Re-centering behavior */
    function resetted() {
        grid.transition()
            .duration(750)
            .call(zoom.transform, d3.zoomIdentity);
    }

    /* * * * * * * * * * *
     * Behaviors
     */

    // copyright year
    d3.select('.year').text(new Date().getFullYear());

    /** The orientation compass */
    const compass = d3.select('#compass div');

    // Compass to "up" labels
    d3.selectAll('#north, #east, #south, #west').on('click.compass', () => compass.text('Up'));

    // Compass to "north" labels
    d3.selectAll('#above, #below').on('click.compass', () => compass.text('N'));

    /** Input direction, defaults to above */
    let perspective = 4;
    d3.select('#north').on('click.perspective', () => {
        perspective = 0;
        liveUpdate();
    });
    d3.selectAll('#east, #below').on('click.perspective', () => {
        perspective = 1;
        liveUpdate();
    });
    d3.select('#south').on('click.perspective', () => {
        perspective = 2;
        liveUpdate();
    });
    d3.select('#west').on('click.perspective', () => {
        perspective = 3;
        liveUpdate();
    });
    d3.select('#above').on('click.perspective', () => {
        perspective = 4;
        liveUpdate();
    });

    /** App */
    const app = d3.select('#app')
        .on('mousedown', () => {
            app.style('cursor', 'auto');
            switch (resetState) {
                case 'edit':
                    if (instructionsState !== resetState) instructionsEdit();
                    break;
                case 'pickup':
                    if (instructionsState !== resetState) instructionsPickup();
                    break;
                default:
                    if (instructionsState !== resetState) instructionsReset();                    
            }
        });

    /** Selection tiles */
    const selectorTiles = d3.selectAll('#tile-selector .tile')
        .on('click', () => {
            let _this = d3.select(d3.event.target);
            app.style('cursor', 'url(./img/' + _this.attr('class').replace('tile', '').trim() + '.png) 32 32, auto');
            instructionsPlace();
        });

    /** Clear button */
    const clear = d3.select('#clear')
        .on('click', () => {
            if (text) {
                text.remove();
                text = null;
            }
            loadPattern(gridDataInit());
            resetted();
        });
    /** Instructions Toggler */
    const toggle = d3.select('#toggle')
        .on('click', () => {
            if (!text) {
                tiles.each((d, i, nodes) => {
                    let _this = d3.select(nodes[i]);
                    if (/\.png/.test(_this.html())) newInstruction(_this, d);
                });
                text = d3.selectAll('text');
            } else {
                text.remove();
                text = null;
            }
        });

    /** Save string */
    const layoutName = d3.select('#save-name');
    /** Load selector */
    const layoutSelector = d3.select('#layout-selector')
    let layouts = {
        'Blue Sample': [[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":2,"img":"./img/purple.png"},{"phi":0,"img":"./img/blue.png"},{"phi":0,"img":"./img/blue.png"},{"phi":0,"img":"./img/lt-blue.png"},{"phi":5,"img":"./img/lt-blue.png"},{"phi":0,"img":"./img/blue.png"},{"phi":0,"img":"./img/blue.png"},{"phi":-1,"img":"./img/purple.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":-1,"img":"./img/blue.png"},{"phi":2,"img":"./img/brown.png"},{"phi":3,"img":"./img/brown.png"},{"phi":1,"img":"./img/orange.png"},{"phi":0,"img":"./img/orange.png"},{"phi":2,"img":"./img/brown.png"},{"phi":3,"img":"./img/brown.png"},{"phi":1,"img":"./img/blue.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":1,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":-1,"img":"./img/blue.png"},{"phi":1,"img":"./img/brown.png"},{"phi":2,"img":"./img/yellow.png"},{"phi":2,"img":"./img/lt-green.png"},{"phi":-1,"img":"./img/lt-green.png"},{"phi":3,"img":"./img/yellow.png"},{"phi":0,"img":"./img/brown.png"},{"phi":1,"img":"./img/blue.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":"./img/lt-blue.png"},{"phi":3,"img":"./img/orange.png"},{"phi":2,"img":"./img/lt-green.png"},{"phi":4,"img":"./img/green.png"},{"phi":5,"img":"./img/green.png"},{"phi":3,"img":"./img/lt-green.png"},{"phi":-2,"img":"./img/orange.png"},{"phi":1,"img":"./img/lt-blue.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":-1,"img":"./img/lt-blue.png"},{"phi":0,"img":"./img/orange.png"},{"phi":1,"img":"./img/lt-green.png"},{"phi":7,"img":"./img/green.png"},{"phi":2,"img":"./img/green.png"},{"phi":0,"img":"./img/lt-green.png"},{"phi":1,"img":"./img/orange.png"},{"phi":2,"img":"./img/lt-blue.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":-1,"img":"./img/blue.png"},{"phi":2,"img":"./img/brown.png"},{"phi":1,"img":"./img/yellow.png"},{"phi":1,"img":"./img/lt-green.png"},{"phi":0,"img":"./img/lt-green.png"},{"phi":0,"img":"./img/yellow.png"},{"phi":3,"img":"./img/brown.png"},{"phi":1,"img":"./img/blue.png"},{"phi":2,"img":null},{"phi":2,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":-1,"img":"./img/blue.png"},{"phi":1,"img":"./img/brown.png"},{"phi":0,"img":"./img/brown.png"},{"phi":2,"img":"./img/orange.png"},{"phi":3,"img":"./img/orange.png"},{"phi":1,"img":"./img/brown.png"},{"phi":0,"img":"./img/brown.png"},{"phi":1,"img":"./img/blue.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":1,"img":"./img/purple.png"},{"phi":-2,"img":"./img/blue.png"},{"phi":-2,"img":"./img/blue.png"},{"phi":-1,"img":"./img/lt-blue.png"},{"phi":2,"img":"./img/lt-blue.png"},{"phi":-2,"img":"./img/blue.png"},{"phi":-2,"img":"./img/blue.png"},{"phi":0,"img":"./img/purple.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}]],
        'Bright Path Sample': [[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":1,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":-2,"img":"./img/lt-green.png"},{"phi":1,"img":"./img/yellow.png"},{"phi":2,"img":"./img/white.png"},{"phi":-1,"img":"./img/white.png"},{"phi":0,"img":"./img/yellow.png"},{"phi":-1,"img":"./img/lt-green.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":1,"img":"./img/lt-green.png"},{"phi":1,"img":"./img/yellow.png"},{"phi":1,"img":"./img/white.png"},{"phi":0,"img":"./img/white.png"},{"phi":0,"img":"./img/yellow.png"},{"phi":0,"img":"./img/lt-green.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":-2,"img":"./img/lt-green.png"},{"phi":1,"img":"./img/yellow.png"},{"phi":2,"img":"./img/white.png"},{"phi":-1,"img":"./img/white.png"},{"phi":0,"img":"./img/yellow.png"},{"phi":-1,"img":"./img/lt-green.png"},{"phi":0,"img":null},{"phi":1,"img":null},{"phi":1,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":1,"img":"./img/lt-green.png"},{"phi":1,"img":"./img/yellow.png"},{"phi":1,"img":"./img/white.png"},{"phi":0,"img":"./img/white.png"},{"phi":0,"img":"./img/yellow.png"},{"phi":0,"img":"./img/lt-green.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":-2,"img":"./img/lt-green.png"},{"phi":1,"img":"./img/yellow.png"},{"phi":2,"img":"./img/white.png"},{"phi":-1,"img":"./img/white.png"},{"phi":0,"img":"./img/yellow.png"},{"phi":-1,"img":"./img/lt-green.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":1,"img":"./img/lt-green.png"},{"phi":1,"img":"./img/yellow.png"},{"phi":1,"img":"./img/white.png"},{"phi":0,"img":"./img/white.png"},{"phi":0,"img":"./img/yellow.png"},{"phi":0,"img":"./img/lt-green.png"},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":2,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":1,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}],[{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null},{"phi":0,"img":null}]]
    };
    for (key in layouts) {
        layoutSelector.append('option')
            .attr('value', key)
            .text(key);
    }
    for (i = 0; i < localStorage.length; i ++) {
        let key = localStorage.key(i);
        layoutSelector.append('option')
            .attr('value', key)
            .text(key);
        layouts[key] = JSON.parse(localStorage.getItem(key));
    }
    const loadButton = d3.select('#load-layout')
        .on('click', () => loadPattern(layouts[layoutSelector.node().value]));
    /** Save button */
    const save = d3.select('#save-layout')
        .on('click', () => {
            const name = layoutName.node().value;
            let data = reduceLayout(rows.data());
            if (name) {
                localStorage.setItem(name, JSON.stringify(data));
                layouts[name] = data;
                layoutSelector.append('option')
                    .attr('value', name)
                    .text(name);
            }
        });

    /** Reduce data */
    function reduceLayout (data) {
        return data.map((r) => r.map((c) => {
            delete c.x;
            delete c.y;
            return c;
        }));
    }

    // update instructions in grid
    grid.on('mouseenter', () => {
        resetState = 'edit';
        if (!instructionsState) instructionsEdit();
    }).on('mouseleave', () => {
        resetState = undefined;
        if (instructionsState === 'edit') instructionsReset();
    });

    // update instructions in tile selector
    tileSelector.on('mouseenter', () => {
        resetState = 'pickup';
        if (!instructionsState) instructionsPickup();
    }).on('mouseleave', () => {
        resetState = undefined;
        if (instructionsState === 'pickup') instructionsReset();
    });

    /** Load grid with new data */
    function loadPattern (data, noReset) {
        // generate postitional data
        let x = -SIZE * 5;
        let y = -SIZE * 6;
        // copy the array from memory so they don't edit them until saved
        map = JSON.parse(JSON.stringify(data)).map((r) => {
            let newRow = r.map((c) => {
                c.x = x;
                c.y = y;
                x += SIZE;
                return c;
            });
            x = -SIZE * 5;
            y += SIZE;
            return newRow;
        });
        // destroy old grid
        grid.selectAll('g').remove().exit();
        // rebuild from scatch with new data
        rows = grid.selectAll()
            .data(map)
            .enter()
            .append('g')
        tiles = rows.selectAll()
            .data((d) => d)
            .enter()
            .append('g');
        tiles.append('rect')
            .attr('x', (d) => d.x)
            .attr('y' , (d) => d.y)
            .attr('width', SIZE)
            .attr('height', SIZE);
        tiles.append('image')
            .attr('x', (d) => d.x)
            .attr('y' , (d) => d.y)
            .attr('width', SIZE)
            .attr('height', SIZE)
            .attr('transform', (d) => `rotate(${d.phi * 90} ${(d.x + SIZE / 2)} ${(d.y + SIZE / 2)})`)
            .attr('href', (d) => d.img? d.img : null)
            .on('click', (d) => {
                let _this = d3.select(d3.event.target);
                if (instructionsState === 'place') { // give tile
                    _this.attr('href', app.style('cursor').match(/\..*\.png/)[0])
                        .attr('transform', null);
                    d.img = app.style('cursor').match(/\..*\.png/)[0];
                    d.phi = 0;
                } else { // CCW
                    d.phi --;
                    _this.attr('transform', `rotate(${d.phi * 90} ${(d.x + SIZE / 2)} ${(d.y + SIZE / 2)})`);
                }
                liveUpdate();
            })
            .on('contextmenu', (d) => { // CW
                d3.event.preventDefault();
                if (instructionsState === 'place') app.style('cursor', 'auto');
                else {
                    d.phi ++;
                    let _this = d3.select(d3.event.target);
                    _this.attr('transform', `rotate(${d.phi * 90} ${(d.x + SIZE / 2)} ${(d.y + SIZE / 2)})`);
                    liveUpdate();
                }
            })
            .on('mousedown', (d) => { // middle click delete
                d3.event.preventDefault();
                let _this = d3.select(d3.event.target);
                if (d3.event.button === 1) {
                    _this.attr('href', null);
                    d.img = null;
                }
                liveUpdate();
            });
        gX = grid.append('g')
            .call(xAxis)
            .selectAll('text')
            .remove()
        gY = grid.append('g')
            .call(yAxis)
            .selectAll('text')
            .remove();
        // Keep the cardinal direction instructions and re-center
        liveUpdate();
        resetted();
    }

    /** Update Instructions Live */
    function liveUpdate () {
        if (text) {
            text.remove();
            tiles.each((d, i, nodes) => {
                let _this = d3.select(nodes[i]);
                if (/.png/.test(_this.html())) newInstruction(_this, d);
            });
            text = d3.selectAll('text');
        }
    }

    /** Create new text element */
    function newInstruction (selection, datum) {
        selection.insert('text')
            .attr('x', (datum.x + SIZE / 2))
            .attr('y', (datum.y + SIZE / 2))
            .text(getDirection(perspective, datum.phi * 90));
    }
    
    /**
     * Set the mouse instructions for the controls.
     * Pass `null` to have the mouse button do nothing.  
     * Arguments' type:
     * ```
     * {
     *   img: <boolean>
     *   value: <value or source>
     * }
     * ```
     */
    function setInstructions (right, left, middle) {
        [rClick, lClick, mClick].forEach((e, i) => {
            let arg = arguments[i]
            if (arg) {
                e.classed('null', false)
                if (arg.img) {
                    e.classed('symbol', true)
                        .html('')
                        .append('img')
                        .attr('src', arg.value);
                }
                else {
                    e.classed('symbol', false)
                        .html(arg.value);
                }
            }
            else {
                e.classed('null', true)
                    .classed('symbol', false)
                    .html('Nothing');
            }
        });
    }

    function instructionsPickup () {
        instructionsState = 'pickup';
        setInstructions(null, {img: false, value: 'Pick Up'}, false);
    }

    function instructionsPlace () {
        instructionsState = 'place';
        setInstructions({img: false, value: 'Drop'}, {img: false, value: 'Place'}, {img: false, value: 'Drop'});
    }

    function instructionsEdit () {
        instructionsState = 'edit';
        setInstructions({img: true, value: './img/cw.svg'}, {img: true, value: './img/ccw.svg'}, {img: false, value: 'Delete'});
    }

    function instructionsReset () {
        instructionsState = undefined;
        setInstructions(null, null, null);
    }

    // mouseover behaviors on whole layout editor
    d3.selectAll('#move-layout div').on('mouseenter', () => {
        resetState = 'edit';
        if (!instructionsState) instructionsEdit();
    }).on('mouseleave', () => {
        resetState = undefined;
        if (instructionsState === 'edit') instructionsReset();
    });
    // Re-center grid
    d3.select('#center-grid').on('click', () => resetted());
    // Shift whole pattern up
    d3.select('#move-up').on('click', () => {
        let data = reduceLayout(rows.data());
        data.shift();
        data.push(new Array(GRID).fill({phi: 0, img: null}));
        loadPattern(data);
    });
    // Shift whole pattern down
    d3.select('#move-down').on('click', () => {
        let data = reduceLayout(rows.data());
        data.pop();
        data.unshift(new Array(GRID).fill({phi: 0, img: null}));
        loadPattern(data);
    });
    // Shift whole pattern right
    d3.select('#move-right').on('click', () => {
        let data = reduceLayout(rows.data());
        data = data.map((r) => {
            r.pop();
            r.unshift({phi: 0, img: null});
            return r;
        });
        loadPattern(data);
    });
    // Shift whole pattern left
    d3.select('#move-left').on('click', () => {
        let data = reduceLayout(rows.data());
        data = data.map((r) => {
            r.shift();
            r.push({phi: 0, img: null});
            return r;
        });
        loadPattern(data);
    });
    // Rotate whole pattern counter-clockwise
    d3.select('#rotate-ccw').on('click', () => {
        let oldData = reduceLayout(rows.data());
        let newData = new Array(GRID).fill(new Array(GRID).fill(undefined));
        newData = newData.map((r, i) => r.map((c, j) => {
            c = oldData[j][GRID - 1 - i];
            c.phi --;
            return c;
        }));
        loadPattern(newData);
    });
    // rotate whole pattern clockwise
    d3.select('#rotate-cw').on('click', () => {
        let oldData = reduceLayout(rows.data());
        let newData = new Array(GRID).fill(new Array(GRID).fill(undefined));
        newData = newData.map((r, i) => r.map((c, j) => {
            c = oldData[GRID - 1 - j][i];
            c.phi ++;
            return c;
        }));
        loadPattern(newData);
    });
});