# Terracotta

Simple D3-powered tool to plan out glazed terracotta designs and provide
cardinal-direction placement instructions to quickly assemble the design
in-game.

[Site](http://saveriodesign.gitlab.io/terracotta)